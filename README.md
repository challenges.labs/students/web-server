><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Instructeur : **Carlin FONGANG**  |  Email : **fongangcarlin@gmail.com**

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______



## Challenge Informatique : Installation de VirtualBox, Ubuntu Server, Apache et WordPress



>![alt text](Install-WP-with-apache.png)

_______

**Bienvenue dans ce challenge informatique !**

**Dans ce travail pratique, vous allez apprendre à configurer votre propre environnement de développement wordpress.**

### Prérequis

- Posseder un ordinateur

- Avoir un accès à internet


**Voici les étapes que vous devrez suivre :**

### Étape 1 : Installation de VirtualBox

VirtualBox est un logiciel de virtualisation produit par Oracle Corporation. Il permet aux utilisateurs de créer et de gérer des machines virtuelles (VM) sur un seul ordinateur physique. En d'autres termes, VirtualBox vous permet de faire fonctionner plusieurs systèmes d'exploitation simultanément sur votre ordinateur.

Par exemple, si vous utilisez un ordinateur Windows, vous pouvez utiliser VirtualBox pour créer une machine virtuelle qui exécute Linux, MacOS, ou une autre version de Windows, sans avoir à redémarrer votre ordinateur ou modifier votre système d'exploitation principal. Chaque machine virtuelle fonctionne de manière isolée et indépendante, avec ses propres ressources virtuelles (CPU, mémoire, disque dur, réseau, etc.).

VirtualBox est particulièrement utile pour les tests logiciels, la gestion de serveurs, l'éducation, et la simulation de réseaux ou de scénarios informatiques sans affecter le système d'exploitation hôte. C'est un outil gratuit et open source, largement utilisé à des fins de développement, de démonstration et de déploiement.


**Objectif** : Installer VirtualBox sur votre machine.

1. Rendez-vous sur le site officiel de VirtualBox : [Télécharger VirtualBox](https://www.virtualbox.org/wiki/Downloads).
2. Sélectionnez la version de VirtualBox correspondant à votre système d'exploitation (Windows, MacOS, Linux, etc.).
3. Téléchargez et installez VirtualBox en suivant les instructions d'installation propres à votre système.

**Ressources** :
- [Guide d'installation VirtualBox](https://www.virtualbox.org/manual/UserManual.html#installation)



### Étape 2 : Installation de Ubuntu Server sur VirtualBox

Ubuntu Server est une variante du système d'exploitation Ubuntu, conçue spécifiquement pour les serveurs. Sans interface graphique par défaut, elle est optimisée pour fonctionner sur des serveurs et des environnements cloud, offrant un support étendu et des mises à jour de sécurité. Idéale pour héberger des sites web, des applications, des bases de données, et plus, elle est reconnue pour sa stabilité, sa sécurité et sa facilité d'utilisation.


**Objectif** : Créer une machine virtuelle (VM) et installer Ubuntu Server 22.04 LTS.

1. Téléchargez l'image ISO d'Ubuntu Server 22.04 LTS depuis le site officiel : [Télécharger Ubuntu Server](https://ubuntu.com/download/server).
2. Ouvrez VirtualBox et créez une nouvelle machine virtuelle :
   - Cliquez sur "Nouvelle".
   - Nommez votre VM (par exemple, "UbuntuServer") et sélectionnez le type "Linux" et la version "Ubuntu (64-bit)".
   - Attribuez à la VM une quantité appropriée de mémoire vive (RAM) – au minimum 1024 MB.
   - Suivez les instructions pour créer un disque dur virtuel – choisissez un fichier VDI avec au moins 10 GB d'espace.
3. Démarrez la VM et, lorsqu'il vous est demandé un disque de démarrage, sélectionnez l'image ISO d'Ubuntu Server que vous avez téléchargée.
4. Suivez les instructions d'installation d'Ubuntu. Créez un utilisateur, sélectionnez la langue, configurez le réseau, etc.

**Ressources** :
- [Guide d'installation d'Ubuntu Server](https://ubuntu.com/server/docs/installation)



### Étape 3 : Installation d'un Serveur Web (Apache, Nginx, ou autre)

Un serveur web est un logiciel et un matériel informatique qui accepte les demandes via HTTP, le protocole utilisé pour distribuer des informations sur le World Wide Web, ou son successeur sécurisé HTTPS. Ils hébergent des sites web, permettant l'accès à des pages web et à leur contenu (texte, images, vidéos) aux utilisateurs via internet. Les serveurs web les plus courants incluent Apache, Nginx et Microsoft IIS. Ils jouent un rôle crucial dans l'hébergement web et la livraison de contenu en ligne.


**Objectif** : Installer un serveur web sur Ubuntu Server.

1. Connectez-vous à votre Ubuntu Server via la console VirtualBox.
2. Ouvrez le terminal et mettez à jour la liste de paquets :
   ```bash
   sudo apt update
   sudo apt upgrade
   ```
3. Installez le serveur web Apache (vous pouvez choisir Nginx ou un autre serveur web si vous préférez) :
   ```bash
   sudo apt install apache2
   ```
4. Vérifiez que le serveur fonctionne en ouvrant votre navigateur et en visitant `http://<IP-de-votre-VM>`. Vous devriez voir la page par défaut d'Apache.

**Ressources** :
- [Apache Ubuntu Documentation](https://ubuntu.com/server/docs/web-servers-apache)



### Étape 4 : Installation de WordPress

**Objectif** : Installer WordPress sur votre serveur web.

1. Installez MySQL (ou MariaDB) et PHP sur votre Ubuntu Server :
   ```bash
   sudo apt install mysql-server php libapache2-mod-php php-mysql
   ```
2. Sécurisez votre installation MySQL et créez une base de données pour WordPress.
3. Téléchargez et configurez WordPress :
   - Téléchargez la dernière version de WordPress :
     ```bash
     cd /var/www/html
     sudo wget https://wordpress.org/latest.tar.gz
     sudo tar -xzvf latest.tar.gz
     ```
   - Configurez WordPress en créant un fichier de configuration basé sur l'exemple fourni et suivez les instructions d'installation.
4. Terminez l'installation en accédant à `http://<IP-de-votre-VM>/wordpress` et en suivant le processus d'installation WordPress.

**Ressources** :
- [WordPress Installation Guide](https://wordpress.org/support/article/how-to-install-wordpress/)


**Conseil** : Prenez votre temps et suivez les instructions étape par étape. Si vous rencontrez des problèmes, n'hésitez pas à rechercher de l'aide en ligne ou à consulter les documentations fournies. **Bonne chance !**

NB. : Produisez une documentation présentant les étapes que vous avez suivies pour réaliser l'exercice (avec captures d'écran).